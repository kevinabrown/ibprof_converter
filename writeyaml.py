#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright 2014 Kevin A. Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


import os

def gen_all(fname, subnet, hosts, pcounter, xdim, ydim):
    import sys
    sys.stderr.write("Generating YAML files...\n")

    gen_meta(fname, xdim, ydim)
    id_map = gen_nodes(fname, subnet, hosts)
    gen_map(fname, hosts, id_map)
    gen_links(fname, subnet, pcounter)

def gen_meta(fname, xdim, ydim):
    yaml_meta = open(fname + '_meta.yaml', 'w')
    yaml_id = os.path.basename(fname)

    text = (""
        "---\n"
        "key: KEVIN_YAML_" + yaml_id + "\n"
        "date: 2013-12-03\n"
        "autour: kevin\n"
        "hardware: {\n"
        " coords: [x, y, z],\n"
        " coords_table: " + yaml_id + "_nodes.yaml,\n"
        " dim: {x: " + str(xdim) + ", y: " + str(ydim) + ", z: 1},\n"
        " source_coords: {x: sx, y: sy, z: sz},\n"
        " destination_coords: {x: tx, y: ty, z: tz},\n"
        " link_duplication_index: did,\n"
        " link_coords_table: " + yaml_id + "_links.yaml\n"
        "}\n"

        "---\n"
        "filetype: table\n"
        "filename: " + yaml_id + "_nodes.yaml\n"
        "domain: HW\n"
        "type: NODE\n"
        "field: nodeid\n"
        "flags: 0\n"

        "---\n"
        "filetype: table\n"
        "filename: " + yaml_id + "_links.yaml\n"
        "domain: HW\n"
        "type: LINK\n"
        "field: linkid\n"
        "flags: 0\n"

        "---\n"
        "filetype: projection\n"
        "type: file\n"
        "filename: " + yaml_id + "_map.yaml\n"
        "subdomain:\n"
        "- { domain: HW, type: NODE, field: nodeid }\n"
        "- { domain: COMM, type: RANK, field: mpirank }\n"
        "flags: 0\n"

        "---\n"
        "filetype: projection\n"
        "type: node links\n"
        "node_policy: Both\n"
        "link_policy: Both\n"
        "subdomain:\n"
        "- { domain: HW, type: NODE, field: nodeid }\n"
        "- { domain: HW, type: LINK, field: linkid }\n"
        "flags: 0\n"

        "...")
    yaml_meta.write(text)
    yaml_meta.close()

def gen_map(fname, hosts, id_map):
    yaml_map = open(fname + '_map.yaml', 'w')
    yaml_id = os.path.basename(fname)

    text = (""
        "---\n"
        "key: KEVIN_YAML_" + yaml_id + "\n"
        "---\n"
        "- [nodeid, int32]\n"
        "- [mpirank, int32]\n"
        "...\n")

    for hname in hosts.keys():
        for pid in hosts[hname]['processes']:
            text += ("" + id_map[hname] + " " + str(pid) + "\n")

    yaml_map.write(text)
    yaml_map.close()

def gen_nodes(fname, subnet, hosts):
    yaml_nodes = open(fname + '_nodes.yaml', 'w')
    yaml_id = os.path.basename(fname)

    id_map = {}
    i = 0

    text = (""
        "---\n"
        "key: KEVIN_YAML_" + yaml_id + "\n"
        "---\n"
        "- [nodeid, int32]\n"
        "- [name, S20]\n"
        "- [x, int32]\n"
        "- [y, int32]\n"
        "- [z, int32]\n"
        "- [load, int32]\n"
        "...\n")

    # Get list of HCAs
    hcaList = [hca for hname in hosts.keys() for hca in hosts[hname].keys() \
                    if hca != 'processes']

    # Add hosts as nodes
    for hname in hosts.keys():
        nodeid = str(i) #str(abs(hosts[hname]['ypos'])) + str(abs(hosts[hname]['xpos']))
        text += (nodeid + " " + hname + " " +
            str(hosts[hname]['xpos']) + " " +
            str(hosts[hname]['ypos']) + " " +
            " 0 1\n")
        id_map[hname] = str(i)
        i += 1

    # Add switches as nodes
    for sid in range(len(subnet)):
        for nguid in subnet[sid].keys():
            if hcaList.count(nguid) == 0:
                text += (str(i) + " " + nguid + " " +
                    str(subnet[sid][nguid]['xpos']) + " " +
                    str(subnet[sid][nguid]['ypos']) + " " +
                    "0 1\n")
                i += 1

    yaml_nodes.write(text)
    yaml_nodes.close()

    return id_map

def gen_links(fname, subnet, pcounter):
    yaml_links = open(fname + '_links.yaml', 'w')
    yaml_id = os.path.basename(fname)

    text = (""
        "---\n"
        "key: KEVIN_YAML_" + yaml_id + "\n"
        "---\n"
        "- [linkid, int32]\n"
        "- [sx, int32]\n"
        "- [sy, int32]\n"
        "- [sz, int32]\n"
        "- [tx, int32]\n"
        "- [ty, int32]\n"
        "- [tz, int32]\n"
        "- [did, int32]\n"
        "- [app_traffic, int64]\n"
        "- [background_traffic, int64]\n"
        "- [code_region, int32]\n"
        "...\n")

    i = 0
    for sid in range(len(subnet)):
        links = []
        id_map = {}
        link_key = {}
        processed = {}
        for nguid in subnet[sid].keys():
            for pn in subnet[sid][nguid].keys():
                if not isinstance(pn, int):
                    continue

                pguid = subnet[sid][nguid][pn]['pguid']
                rpn = subnet[sid][nguid][pn]['rpn']
                rnguid = subnet[sid][nguid][pn]['rnguid']
                rpguid = subnet[sid][rnguid][rpn]['pguid']

                sx = subnet[sid][nguid]['xpos']
                sy = subnet[sid][nguid]['ypos']
                tx = subnet[sid][rnguid]['xpos']
                ty = subnet[sid][rnguid]['ypos']

                src = (sx, sy, 0)
                dst = (tx, ty, 0)

                if not link_key.has_key((src, dst)):
                    link_key[(src, dst)] = {}

                # Ensure list is never empty. Add initializer
                dup_list = [0]
                if link_key.has_key((dst, src)):
                    if link_key[(dst, src)].has_key((rpguid, rpn)):
                        # If the reverse direction of this link was already
                        # added, use the same dup_id
                        link_key[(src, dst)][(pguid, pn)] = \
                                    link_key[(dst, src)][(rpguid, rpn)]
                    else:
                        # get dup_id numbers in reverse direction
                        dup_list.extend(link_key[(dst, src)].values())

                # get dup_id numbers in forward direction
                dup_list.extend(link_key[(src, dst)].values())

                if not link_key[(src, dst)].has_key((pguid, pn)):
                    # if the list has more values besides our initializer,
                    # add 1 to the max value, else use '0'
                    val = max(dup_list) + 1 if len(dup_list) > 1 else 0
                    link_key[(src, dst)][(pguid, pn)] = val

                dups = link_key[(src, dst)][(pguid, pn)]

                traffic = subnet[sid][nguid][pn]['edgeWeight']

                # get transmit counter for port
                if pcounter[sid] != None:
                    if pcounter[sid].has_key(pguid):
                        bkgd = pcounter[sid][pguid][pn]['xmit']
                    else:
                        bkgd = 0
                else:
                    bkgd = 0

                # links element: (id, src, dst, did, data, bkgd, region)
                for (region, data) in traffic.items():
                    if region == 0:
                        links.append((i, src, dst, dups, 0, bkgd, region))
                    else:
                        links.append((i, src, dst, dups, data, 0, region))

                i += 1

        # write links to files text
        for (id, src, dst, dx, data, bkgd, region) in links:
            (sx, sy, sz) = src
            (tx, ty, tz) = dst
            did = str(dx)
            text += (str(id) + " " +
                    str(sx) + " " + str(sy) + " " + str(sz) + " " +
                    str(tx) + " " + str(ty) + " " + str(tz) + " " +
                    str(did) + " " + str(data) + " " + str(bkgd) +
                    " " + str(region) +
                    "\n")

    yaml_links.write(text)
    yaml_links.close()
