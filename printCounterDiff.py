#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright 2014 Kevin A. Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import os, sys, pmcounter, glob
from pprint import pprint as pp

sys.stderr.write("Getting port counters from " + sys.argv[1] + " ...\n")

rail_dir = sys.argv[1]
pcounter = []

pmfile_a = glob.glob(os.path.normpath(rail_dir + '/*.pm' ))
pmfile_b = glob.glob(os.path.normpath(rail_dir + '/*.pm2'))

if len(pmfile_a) <= 0 or len(pmfile_b) <= 0:
    sys.stderr.write("[WARNING]Pre and/or post-run port counters could not be found.\n")
    pcounter.append(None)
    exit()

pm1 = pmcounter.parsefile(pmfile_a[0])
pm2 = pmcounter.parsefile(pmfile_b[0])
pm0 = pmcounter.getdiff(pm1, pm2)

pcounter.append(pm0)

pp(pcounter)
