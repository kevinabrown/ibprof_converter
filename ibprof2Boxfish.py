#!/usr/bin/env python

"""
Copyright 2014 Kevin A. Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import sys, re, os, random
import glob, ast

from operator import itemgetter
from collections import deque as queue
from pprint import pprint

import ibprofparser, pmcounter, writeyaml

class ibprof2Boxfish(object):
    subnet          = []
    hcaTable        = []
    hosts           = {}
    maxEdgeWeight   = 1

    pcounter        = []
    messages        = []
    processes       = []

    roots           = []
    hcaList         = []
    max_ypos        = []
    max_xpos        = []
    max_region      = 0         # region 0 is reserved for port counters

    level1_q        = queue([])
    level2_q        = queue([])
    level3_q        = queue([])

    """ Get the node and port connection information of all nodes and ports in the network
    """
    def parseLSTFile(self, fileName='', sid=0):
        sys.stderr.write("Parsing " + fileName + " ...\n")

        lstFile = open( fileName, 'r' )

        network = {}

        for line in lstFile:
            p = re.compile('{\s+([a-zA-Z0-9_-]+)\s+Ports:(\w+)\s+SystemGUID:(\w+)\s+NodeGUID:(\w+)\s+PortGUID:(\w+)\s+VenID:(\w+)\s+DevID:(\w+)\s+Rev:(\w+)\s+{(\w+)(.+)}\s+LID:(\w+)\s+PN:(\w+)\s+}\s+{\s+([a-zA-Z0-9_-]+)\s+Ports:(\w+)\s+SystemGUID:(\w+)\s+NodeGUID:(\w+)\s+PortGUID:(\w+)\s+VenID:(\w+)\s+DevID:(\w+)\s+Rev:(\w+)\s+{(\w+)(.+)}\s+LID:(\w+)\s+PN:(\w+)\s+}\s+.+')
            if p.match(line):
                m = p.match(line)
                node1, ports1, sguid1, nguid1, pguid1, vid1, did1, rev1, hname1, dname1, lid1, pn1 = m.group(1), int(m.group(2),16), m.group(3), m.group(4), m.group(5), \
                                                                                                     m.group(6), m.group(7), m.group(8), m.group(9), m.group(10), int(m.group(11),16), int(m.group(12),16)
                node2, ports2, sguid2, nguid2, pguid2, vid2, did2, rev2, hname2, dname2, lid2, pn2 = m.group(13), int(m.group(14),16), m.group(15), m.group(16), m.group(17), \
                                                                                                     m.group(18), m.group(19), m.group(20), m.group(21), m.group(22), int(m.group(23),16), int(m.group(24),16)

                nguid1, nguid2 = nguid1.lower(), nguid2.lower()

                if node1.find('CA') > -1:
                    node1, ports1, sguid1, nguid1, pguid1, vid1, did1, rev1, hname1, dname1, lid1, pn1, node2, ports2, sguid2, nguid2, pguid2, vid2, did2, rev2, hname2, dname2, lid2, pn2 = \
                            node2, ports2, sguid2, nguid2, pguid2, vid2, did2, rev2, hname2, dname2, lid2, pn2, node1, ports1, sguid1, nguid1, pguid1, vid1, did1, rev1, hname1, dname1, lid1, pn1
                if node2.find('CA') > -1:
                    if self.hosts.has_key(hname2):
                        if self.hosts[hname2].has_key(nguid2):
                            self.hosts[hname2][nguid2][pn2] = {pguid2: lid2}
                        else:
                            self.hosts[hname2][nguid2] = {pn2: {pguid2: lid2}}
                    else:
                        self.hosts[hname2] = {nguid2: {pn2: {pguid2: lid2}}}
                        self.hosts[hname2]['xpos'] = None
                        self.hosts[hname2]['ypos'] = None
                        self.hosts[hname2]['processes'] = []

                    self.hcaTable.append([nguid2, pn2, lid2, sid])
                    self.hcaList.append([sid, nguid2])

                if network.has_key(nguid1):
                    network[nguid1][pn1] = {'rnguid':nguid2, 'rpn':pn2, 'edgeWeight':{}, 'pguid':pguid1}
                else:
                    network[nguid1] = {'xpos':None}
                    network[nguid1]['ypos'] = None
                    network[nguid1][pn1] = {'rnguid':nguid2, 'rpn':pn2, 'edgeWeight':{}, 'pguid':pguid1}

                if network.has_key(nguid2):
                    network[nguid2][pn2] = {'lid':lid1, 'rnguid':nguid1, 'rpn':pn1, 'edgeWeight':{}, 'pguid':pguid2}
                else:
                    network[nguid2] = {'xpos':None}
                    network[nguid2]['ypos'] = None
                    network[nguid2][pn2] = {'lid':lid2, 'rnguid':nguid1, 'rpn':pn1, 'edgeWeight':{}, 'pguid':pguid2}

        self.hcaList.sort(key=itemgetter(0,1))
        self.subnet.append(network)
        lstFile.close()

    """ Get the port forwading tables of all switches in the network
    """
    def parseFDBSFile(self, fileName='', sid=0):
        sys.stderr.write("Parsing " + fileName + " ...\n")

        network = self.subnet[sid]
        nguid = None
        lft = {}

        fdbsFile = open( fileName, 'r' )
        for line in fdbsFile:
            p = re.compile('osm_ucast_mgr_dump_ucast_routes: Switch 0x(\w+)')
            if p.match(line):
                if nguid:
                    network[nguid]['lft'] = lft
                    lft = {}
                nguid = p.match(line).group(1).lower()

            p = re.compile('0x(\w+)\s+:\s+(\d+)\s+.*')
            if p.match(line):
                lid, port = int(p.match(line).group(1), 16), int(p.match(line).group(2))
                lft[lid] = port

        if nguid:
            network[nguid]['lft'] = lft

        fdbsFile.close()

    """ If all files are available, read the differences in network-wide send and recv
        port counters
    """
    def getPortCounters(self, rail_dir):
        sys.stderr.write("Getting port counters from " + rail_dir + " ...\n")

        pcounter = self.pcounter

        pmfile_a = glob.glob(os.path.normpath(rail_dir + '/*.pm' ))
        pmfile_b = glob.glob(os.path.normpath(rail_dir + '/*.pm2'))

        if len(pmfile_a) <= 0 or len(pmfile_b) <= 0:
            sys.stderr.write("[WARNING]Pre and/or post-run port counters could not be found.\n")
            pcounter.append(None)
            return

        pm1 = pmcounter.parsefile(pmfile_a[0])
        pm2 = pmcounter.parsefile(pmfile_b[0])
        pm0 = pmcounter.getdiff(pm1, pm2)

        pcounter.append(pm0)

    """ Get data from profiles consolidate send and recv messages on the sender
    """
    def importProfile(self, prof_id): #TODO: optimize adding recv messages
        sys.stderr.write("Parsing profile: " + prof_id + ".otf ...\n")

        hosts       = self.hosts
        hcaTable    = self.hcaTable
        network     = self.subnet
        messages    = self.messages
        processes   = self.processes

        ports = {}
        lids = [{} for sid in range(len(self.subnet))]

        for [nguid, pn, lid, sid] in hcaTable:
            pguid = network[sid][nguid][pn]['pguid']

            if not ports.has_key(pguid):
                ports[pguid] = {'sid': sid}

            if not lids[sid].has_key(lid):
                lids[sid][lid] = {'nguid': nguid, 'pn': pn}

        run = ibprofparser.profReader(prof_id)
        prof_data = run.getData()

        for mpirank in prof_data.keys():
            hname = prof_data[mpirank]['hostname'].strip()
            if hname in hosts.keys():
                if hosts[hname]['processes'].count(mpirank) == 0:
                    hosts[hname]['processes'].append(mpirank)
            else:
                print 'Hosts %s not found in subnet(s)' %prof_data[mpirank]['hostname']

            """ Create list of messages """
            for src_info in prof_data[mpirank].keys():
                if src_info != 'hostname':
                    for dst_lid, data in prof_data[mpirank][src_info].items():
                        for op, traffic in data.items():
                            self.max_region = self.max_region if max(traffic.keys()) < \
                                                self.max_region else max(traffic.keys())

                            """ If data is sent, add to list """
                            if op == 'sent':
                                messages.append((src_info, dst_lid, traffic))

                            # If data is recieved, find pguid of dest lid and add
                            # create 'sent_src -> recv_dst' list entry
                            elif op == 'recv':
                                sid = None
                                dst_info = None

                                # Find sid for the subnet that was used
                                sid = ports[src_info[0]]['sid']
                                rpn = lids[sid][dst_lid]['pn']
                                rnguid = lids[sid][dst_lid]['nguid']
                                rpguid = network[sid][rnguid][rpn]['pguid']

                                dst_info = (rpguid, dst_lid)
                                src_lid = src_info[1]

                                messages.append((dst_info, src_lid, traffic))


    """ Determine the horizontal and vertical position of each node
    """
    def findPos(self):
        sys.stderr.write("Positioning nodes.......\n")
        self.findYPos()
        self.findXPos()

        # (1) Pad xpos to distribute nodes horizontally and
        # (2) Invert the level (ypos) of the 2nd subnet
        for sid in range(len(self.subnet)):
            network = self.subnet[sid]
            for nguid in network.keys():
                ypos = network[nguid]['ypos']

                my_max_xpos = self.max_xpos[sid][ypos] - 1 \
                                if (self.max_xpos[sid][ypos] > 0) \
                                   else self.max_xpos[sid][ypos]
                net_max_xpos = max(self.max_xpos[sid])-1  \
                                if (max(self.max_xpos[sid]) > 0) \
                                   else max(self.max_xpos[sid])

                xpos = network[nguid]['xpos'] * (1.0 * net_max_xpos / my_max_xpos)
                network[nguid]['ypos'] = ypos if (sid == 0) else (-1 * ypos)
                network[nguid]['xpos'] = xpos

    """ Finds the vertical position (ypos) of all nodes
        -   Nodes with HCA's are given a ypos of 0
        -   All other nodes are positioned based on their minimum distance away
            from an HCA
    """
    def findYPos(self):
        sys.stderr.write(" calculating Y position.......\n")
        network     = self.subnet
        hcaList     = self.hcaList
        hcaTable    = self.hcaTable
        max_height  = [1 for y in range(len(network))]
        work_q      = queue([])

        """ Label upwards from switches connected to HCA (temp Edge switches)"""
        for hca, port, lid, sid in hcaTable:
            rnguid = network[sid][hca][port]['rnguid']
            network[sid][hca]['ypos'] = 0

            if work_q.count([sid, rnguid]) == 0:
                network[sid][rnguid]['ypos'] = 1
                work_q.append([sid, rnguid])

        visited = list(hcaList)

        while len(work_q) > 0:
            sid, curr = work_q.popleft()
            visited.append([sid, curr])
            if hcaList.count([sid, curr]) > 0:     # this test may not be necessary
                continue

            for port in network[sid][curr].keys():
                if isinstance(port, int):

                    rnguid = network[sid][curr][port]['rnguid']
                    if visited.count([sid, rnguid]) > 0 \
                            or work_q.count([sid, rnguid]) > 0:
                        continue

                    network[sid][rnguid]['ypos'] = network[sid][curr]['ypos'] + 1
                    max_height[sid] = max(network[sid][rnguid]['ypos'], max_height[sid])
                    work_q.append([sid, rnguid])

        """ Place all root switches (nodes without parents) on the topmost level
        """
        for sid in range(len(network)):
            for nguid in network[sid].keys():
                if self.getConnected(sid, nguid, 'parent|peer') == None:
                    network[sid][nguid]['ypos'] = max_height[sid]

        self.max_ypos = max_height


    """ Returns list of parents of a given node or None if none is found
    """
    def getConnected(self, sid, nguid, types):
        network = self.subnet
        my_height = network[sid][nguid]['ypos']
        connected = []

        for port in network[sid][nguid].keys():
            if isinstance(port, int):

                rnguid = network[sid][nguid][port]['rnguid']
                if network[sid][rnguid]['ypos'] > my_height and 'parent' in types:
                    connected.append(rnguid)
                if network[sid][rnguid]['ypos'] == my_height and 'peer' in types:
                    connected.append(rnguid)
                if network[sid][rnguid]['ypos'] < my_height and 'child' in types:
                    connected.append(rnguid)

        if len(connected) > 0:
            return connected
        else:
            return None

    """ Positions nodes horizontally while minimizing the number of
        intersecting links
    """
    def findXPos(self):
        sys.stderr.write(" calculating X position.......\n")

        self.max_xpos = [[0 for x in range(self.max_ypos[sid] + 1)] \
                            for sid in range(len(self.subnet))]

        # Find common hca in all subnets
        hcas = []
        max_sid = len(self.subnet)
        for hname in self.hosts.keys():
            i = 0
            hcas = []
            for key in self.hosts[hname].keys():
                if key != 'xpos' and key != 'ypos' and key != 'processes':
                    i += 1
                    hcas.append(key)

                if i == max_sid:
                    break

            if i == max_sid:
                break

        if i != max_sid:
            sys.exit('No common hca found in all subnets.')

        # Use the first common host as the starting point for both subnets
        for hca in hcas:
            self.level1_q.clear()
            self.level2_q.clear()
            self.level3_q.clear()

            # Find sid of hca and give hca a xpos of '0'
            sid = [id for [id, ca] in self.hcaList if ca == hca][0]
            pos_hcas = {hca: 0}

            # if hcas in this subnet already have positions, seed position of edge switches
            for nguid in self.subnet[sid].keys():
                xpos = self.subnet[sid][nguid]['xpos']
                if self.hcaList.count([sid, nguid]) > 0:
                    if xpos != None:
                        pos_hcas[nguid] = xpos

            for ca in sorted(pos_hcas, key=pos_hcas.get):
                parents = self.getConnected(sid, ca, 'parent')
                if len(parents) > 0:
                    self.level1_q.append((sid, parents[0]))

            if len(self.level1_q) == 0:
                sys.exit('No parents found for hca in a given subnet.')

            self.posLevel1()
            self.posLevel2()
            self.posLevel3()

    # TODO: Combine
    """ Determine the horizontal position of nodes in level0 and level1
        (HCAs and Edge switches)
    """
    def posLevel1(self):
        while len(self.level1_q) > 0:
            (sid, nguid) = self.level1_q.popleft()
            self.assignXPos(sid, nguid)

            children = self.getConnected(sid, nguid, 'child')
            if children != None:
                for child in children:
                    self.assignXPos(sid, child)

                    peers = self.getConnected(sid, child, 'parent|peer')
                    for peer in peers:
                        if self.subnet[sid][peer]['xpos'] == None:
                            self.level1_q.appendleft((sid, peer))

            parents = self.getConnected(sid, nguid, 'parent|peer')
            if parents != None:
                for parent in parents:
                    self.assignXPos(sid, parent)
                    self.level2_q.append((sid, parent))

    """ Determine the horizontal position of nodes in level2
    """
    def posLevel2(self):
        while len(self.level2_q) > 0:
            (sid, nguid) = self.level2_q.popleft()
            self.assignXPos(sid, nguid)

            children = self.getConnected(sid, nguid, 'child')
            if children != None:
                for child in children:
                    if self.subnet[sid][child]['xpos'] == None:
                        self.level1_q.appendleft((sid, child))
                self.posLevel1()

            parents = self.getConnected(sid, nguid, 'parent|peer')
            if parents != None:
                for parent in parents:
                    self.level3_q.append((sid, parent))

    """ Determine the horizontal position of nodes in level3
    """
    def posLevel3(self):
        while len(self.level3_q) > 0:
            (sid, nguid) = self.level3_q.popleft()
            self.assignXPos(sid, nguid)

            children = self.getConnected(sid, nguid, 'child')
            if children != None:
                for child in children:
                    if self.subnet[sid][child]['xpos'] == None:
                        self.level2_q.append((sid, child))
                self.posLevel2()

            peers = self.getConnected(sid, nguid, 'parent|peer')
            if peers != None:
                for peer in peers:
                    if self.subnet[sid][peer]['xpos'] == None:
                        self.assignXPos(sid, peer)
                        self.level3_q.appendleft((sid, peer))

    def assignXPos(self, sid, nguid):
        hosts = self.hosts

        if self.subnet[sid][nguid]['xpos'] != None:
            return

        ypos = self.subnet[sid][nguid]['ypos']

        if ypos > 0:
            self.subnet[sid][nguid]['xpos'] = self.max_xpos[sid][ypos]
            self.max_xpos[sid][ypos] += 1

        elif ypos == 0:
            found = False
            for hname, hca in hosts.items():
                for node, port in hca.items():
                    if nguid == node:
                        found = True
                        break
                if found == True:
                    break

            if found == True:
                if hosts[hname]['xpos'] == None:
                    xpos = self.max_xpos[0][0]
                    hosts[hname]['xpos'] = xpos
                    hosts[hname]['ypos'] = 0
                    for node in hosts[hname].keys():
                        if node != 'xpos' and node != 'ypos' and node != 'processes':
                            net = [id for [id, ca] in self.hcaList if ca == node][0]
                            self.subnet[net][node]['xpos'] = xpos

                    self.max_xpos[0][0] += 1

                else:
                    self.subnet[sid][nguid]['xpos'] = hosts[hname]['xpos']

            if self.subnet[sid][nguid]['xpos'] == None:
                self.subnet[sid][nguid]['xpos'] = self.max_xpos[0][0]
                self.max_xpos[0][0] += 1

            self.max_xpos[sid][0] = self.max_xpos[0][0]


        else:
            print 'Unable to find YPOS for nguid: ', nguid
            sys.exit('')

    """ Follow the paths of messages sent through the network using the port forwarding
        tables and add weights to the links based on the size of the messages
    """
    def updateLinkValues(self):
        sys.stderr.write("Calculating edge weight...\n")
        network  = self.subnet
        messages = self.messages
        hosts    = self.hosts
        hcaTable  = self.hcaTable

        for net in network:
            for nguid in net.keys():
                for pn in net[nguid].keys():
                    if not isinstance(pn, int):
                        continue
                    for x in range(0, self.max_region+1):
                        net[nguid][pn]['edgeWeight'][x] = 0

        def addMsgVals(sid, nguid, pn, traffic):
            for region, data in traffic.items():
                self.subnet[sid][nguid][pn]['edgeWeight'][region] += data

        ports = {}
        lids = [{} for sid in range(len(self.subnet))]

        for [nguid, pn, lid, sid] in hcaTable:
            pguid = network[sid][nguid][pn]['pguid']

            if not ports.has_key(pguid):
                ports[pguid] = {'sid': sid}

            if not lids[sid].has_key(lid):
                lids[sid][lid] = {'nguid': nguid, 'pn': pn}

        """oldout = sys.stdout

        sys.stdout = open("ports.data", "w")
        pprint(ports)
        sys.stdout.close()

        sys.stdout = open("hca.data", "w")
        pprint(hcaTable)
        sys.stdout.close()

        sys.stdout = open("net.data", "w")
        pprint(network)
        sys.stdout.close()

        sys.stdout = open("msg.data", "w")
        pprint(messages)
        sys.stdout.close()

        sys.stdout = oldout
        """

        for port_info, lidDst, traffic in messages:

            pguid, lidSrc = port_info
            sid = ports[pguid]['sid']

            hcaSrc  = lids[sid][lidSrc]['nguid']
            portSrc = lids[sid][lidSrc]['pn']
            hcaDst  = lids[sid][lidDst]['nguid']
            portDst = lids[sid][lidDst]['pn']

            addMsgVals(sid, hcaSrc, portSrc, traffic)

            curr = network[sid][hcaSrc][portSrc]['rnguid']

            while (curr != hcaDst):
                if not network[sid][curr]['lft'].has_key(lidDst):
                    print 'Could not find path for a message!!!!'
                    break
                exitPort = network[sid][curr]['lft'][lidDst]
                addMsgVals(sid, curr, exitPort, traffic)
                curr = network[sid][curr][exitPort]['rnguid']

    """ Maps nguid to hostname, if available
    """
    def nguidToLabel(self, nguid):
        for hname in self.hosts.keys():
            for node in self.hosts[hname].keys():
                if nguid == node:
                    return hname

        return nguid

    """ Writes Boxfish visualization file
    """
    def genYAML(self, run_id):
        xdim = max(max(x) for x in self.max_xpos) + 1
        ydim = max(self.max_ypos) * len(self.subnet) + 1
        writeyaml.gen_all(run_id, self.subnet, self.hosts, self.pcounter, xdim, ydim)


    """ Main function
    """
    def __init__(self):
        sys.setrecursionlimit(10000)
        if len(sys.argv) >= 4:
            rails = []
            for i in range(1, len(sys.argv)-2):
                path, prefix = os.path.split( os.path.normpath(sys.argv[i]) )
                if path == '': path = os.getcwd()
                rail_dir = os.path.join(path, prefix)
                if not os.path.exists(rail_dir):
                    print "Error: dir [%s] does not exist or is not accessable" % sys.argv[i]
                    print ""
                    self.printHelp()
                rails.append(rail_dir)

            prof_id = sys.argv[i+1]
            if not os.path.exists(prof_id + '.otf'):
                print "Error: Profile " + prof_id + ".otf  not found."
                print ""
                self.printHelp()

            run_id = sys.argv[i+2]

        else:
            self.printHelp()

        for rail in rails:
            lstFile = glob.glob(os.path.normpath(rail + '/*.lst'))
            if len(lstFile) > 0:
                self.parseLSTFile(lstFile[0], rails.index(rail))
            else:
                sys.stderr.write("LST file not found.\n")
                sys.exit('')

            fdbsFile = glob.glob(os.path.normpath(rail + '/*.fdbs'))
            if len(fdbsFile) > 0:
                self.parseFDBSFile(fdbsFile[0], rails.index(rail))
            else:
                sys.stderr.write("FDBS file not found.\n")
                sys.exit('')

            self.getPortCounters(rail)

        if len(self.hcaList) <= 0:
            sys.stderr.write("No HCAs found.\n")
            sys.exit('')

        self.importProfile(prof_id)
        self.findPos()

        self.updateLinkValues()
        self.genYAML(run_id)

    def printHelp(self):
        print "Usage: ", sys.argv[0], " <rail_1> <rail_2> <prof_id> <run_id>"
        print ""
        print "     Output:"
        print "         ", sys.argv[0], " writes a dot file for unicast forwarind indizes in *.fdbs"
        print ""
        print "     Example:"
        print "         ", sys.argv[0], " ./tsubame/rail1/ ./tsubame/rail2/ alltoall321 run_101"
        print ""
        print "     Hint to generate *.lst and *.fdbs output:"
        print "         ibdiagnet [-r] -o <outDir>"
        sys.exit('')

if __name__ == "__main__":
    app = ibprof2Boxfish()
    sys.exit('\nFinish!')

